﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace HomeTask1.Interfaces
{
    [ServiceContract]
    public interface ILibraryService
    {
        [OperationContract]
        Book GetBookById(int id);

        [OperationContract]
        Book GetBookByAuthor(string name);

        [OperationContract]
        void AddBook(Book book);

        [OperationContract]
        Book GiveBook(Book book);

        [OperationContract]
        void ReturnBook(Book book);
    }
}
