﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using HomeTask1.Interfaces;

namespace HomeTask1
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerSession)]
    public class LibraryService : ILibraryService
    {
        private ICollection<Book> _books;

        public LibraryService()
        {
            _books = new List<Book>()
            {
                new Book()
                {
                    Id = 1,
                    Author = "Somebody",
                    BookType = Book.BookTypes.Scientific,
                    Name = "C# book",
                    Year = 322
                }
            };
        }

        public Book GetBookById(int id)
        {
            return _books.SingleOrDefault(x => x.Id == id);
        }

        public Book GetBookByAuthor(string author)
        {
            return _books.SingleOrDefault(x => x.Author.Equals(author));
        }

        public void AddBook(Book book)
        {
            _books.Add(book);
        }

        public Book GiveBook(Book book)
        {
            _books.Remove(book);
            return book;
        }

        public void ReturnBook(Book book)
        {
            AddBook(book);
        }
    }

    [DataContract]
    public class Book
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Author { get; set; }

        [DataMember]
        public int Year { get; set; }

        [DataMember]
        public BookTypes BookType { get; set; }

        public enum BookTypes
        {
            /// <summary>
            /// Журнал
            /// </summary>
            Magazine,
            /// <summary>
            /// Художественная книга
            /// </summary>
            Fiction,
            /// <summary>
            /// Научная
            /// </summary>
            Scientific
        }
    }
}