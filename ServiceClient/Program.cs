﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceClient.ServiceReference1;

namespace ServiceClient
{
    public class Program
    {
        static void Main(string[] args)
        {
            var client = new LibraryServiceClient();
            var book = client.GetBookById(1);
            Console.WriteLine(book.Name);

            client.Close();
        }
    }
}
